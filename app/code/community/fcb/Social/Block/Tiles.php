<?php

class Fcb_Social_Block_Tiles extends Mage_Core_Block_Template
{
    const MODULE_CONFIG_SECTION = 'social';
    const MODULE_CONFIG_GLOBAL_GROUP = 'global';
    const MODULE_CONFIG_CACHE_GROUP = 'caching';

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        // populate child blocks with paging params from request
        $pageHash = Mage::app()->getRequest()->getParam('social_pg', null);
        $pagerQueryParams = array();

        if ($pageHash) {
            parse_str(base64_decode($pageHash), $pagerQueryParams);
        }

        foreach ($this->getSortedChildBlocks() as $block) {
            if ($block instanceof Fcb_Social_Block_Feed_Abstract) {
                if (!$block->isEnabled()) {
                    // remove disabled child blocks
                    $this->unsetChild($block->getNameInLayout());
                } elseif (!empty($pagerQueryParams)) {
                    $block->setPagerQueryParams($pagerQueryParams);
                }
            }
        }

        return $this;
    }

    /**
     * Get cache key informative items
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'BLOCK_TPL',
            Mage::app()->getStore()->getCode(),
            $this->getNameInLayout(),
            Mage::app()->getRequest()->getParam('social_pg', null)
        );
    }

    /**
     * Get block cache life time
     *
     * @return int
     */
    public function getCacheLifetime()
    {
        if ($this->hasData('cache_lifetime')) {
            return $this->getData('cache_lifetime');
        }

        return self::isCachingEnabled()
            ? self::getSetting('cache_lifetime', self::MODULE_CONFIG_CACHE_GROUP)
            : 0;
    }

    public function isEnabled()
    {
        return self::getSetting('enabled');
    }

    public static function isCachingEnabled()
    {
        return self::getSetting('enable_caching', self::MODULE_CONFIG_CACHE_GROUP);
    }

    public static function isDebugEnabled()
    {
        return self::getSetting('debug_enabled');
    }

    public static function getSetting($key, $group = self::MODULE_CONFIG_GLOBAL_GROUP)
    {
        return Mage::getStoreConfig(self::MODULE_CONFIG_SECTION . '/' . $group . '/' . $key);
    }

    public function getPageHash()
    {
        return base64_encode($this->_generatePageQueryParams());
    }

    protected function _generatePageQueryParams()
    {
        $params = array();
        $childBlocks = $this->getChild();

        foreach ($childBlocks as $block) {
            if ($block instanceof Fcb_Social_Block_Feed_Abstract && $block->isEnabled()) {
                $params = array_merge($params, $block->getPagerQueryParams());
            }
        }

        return http_build_query($params);
    }

    public function getShowMoreLink()
    {
        if (($pageHash = $this->getPageHash()) != '') {
            return '/social/tiles?' . http_build_query(array(
                'social_pg' => $pageHash,
                'name' => $this->getNameInLayout()
            ));
        }

        return false;
    }
}