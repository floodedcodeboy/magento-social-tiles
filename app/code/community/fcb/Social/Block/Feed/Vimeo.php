<?php

class Fcb_Social_Block_Feed_Vimeo extends Fcb_Social_Block_Feed_Abstract
{
    const CONFIG_GROUP = 'vimeo';
    const PAGER_PARAM = 'vimeo_pg';

    protected $_limit = 20;
    protected $_page = 1;

    public function setPagerQueryParams(array $params)
    {
        if (isset($params[self::PAGER_PARAM])) {
            $this->setPage($params[self::PAGER_PARAM]);
        }

        return $this;
    }

    public function getPagerQueryParams()
    {
        return array(self::PAGER_PARAM => $this->getPage());
    }

    public function setPageLimit($limit)
    {
        $this->_limit = $limit;
        return $this;
    }

    public function getPageLimit()
    {
        return $this->_limit;
    }

    public function setPage($page)
    {
        $this->_page = $page;
        return $this;
    }

    public function getPage()
    {
        return $this->_page;
    }

    public function setNumResults($numResults)
    {
        $this->_numResults = $numResults;
        return $this;
    }

    public function getNumResults()
    {
        return $this->_numResults;
    }

    public function getVideos()
    {
        $feedUrl = sprintf(
            'http://vimeo.com/api/v2/%s/all_videos.json?page=%s',
            self::getSetting('user_id'),
            $this->getPage()
        );

        if ($this->isDebugEnabled()) {
            Mage::log(__METHOD__ . ' :: requesting feed: ' . $feedUrl);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $feedUrl);

        try {
            $videos = curl_exec($curl);
        } catch (Exception $e) {
            if ($this->isDebugEnabled()) {
                Mage::logException($e);
            }

            curl_close($curl);
            return false;
        }

        $videos = json_decode($videos, true);
        $count = is_array($videos) ? count($videos) : 0;
        if ($this->isDebugEnabled()) {
            Mage::log(__METHOD__ . ' :: videos returned: ' . $count . ' result(s)');
        }

        $this->setNumResults($count);
        $this->setPage($count > 0 ? $this->getPage() + 1 : $this->getPage());

        return $videos;
    }
}