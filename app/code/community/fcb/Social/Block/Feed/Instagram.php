<?php

class Fcb_Social_Block_Feed_Instagram extends Fcb_Social_Block_Feed_Abstract
{
    const CONFIG_GROUP = 'instagram';
    const PAGER_PARAM = 'ig_max_id';

    protected $_maxId;
    protected $_maxResults = 24;
    protected $_numResults;
    protected $_userId;

    public function setMaxResults($maxResults)
    {
        $this->_maxResults = $maxResults;
        return $this;
    }

    public function getMaxResults()
    {
        return $this->_maxResults;
    }

    public function setMaxId($maxId)
    {
        $this->_maxId = $maxId;
        return $this;
    }

    public function getMaxId()
    {
        return $this->_maxId;
    }

    public function setUserId($userId)
    {
        $this->_userId = $userId;
        return $this;
    }

    public function getUserId()
    {
        if (!$this->_userId) {
            $this->_userId = self::getSetting('user_id');
        }

        return $this->_userId;
    }

    public function setPagerQueryParams(array $params)
    {
        if (isset($params[self::PAGER_PARAM])) {
            $this->setMaxId($params[self::PAGER_PARAM]);
        }

        return $this;
    }

    public function getPagerQueryParams()
    {
        return array(self::PAGER_PARAM => $this->getMaxId());
    }

    public function setNumResults($numResults)
    {
        $this->_numResults = $numResults;
        return $this;
    }

    public function getNumResults()
    {
        return $this->_numResults;
    }

    public function getPostsByUser($userId = null)
    {
        if ($userId === null) {
            $userId = $this->getUserId();
        }

        return $this->_getPosts('/users/' . $userId .'/media/recent');
    }

    public function getPostsByTag($tag)
    {
        return $this->_getPosts('/tags/' . $tag . '/media/recent');
    }

    protected function _getPosts($endpoint)
    {
        $endpoint = sprintf(
            '%s?access_token=%s&count=%s&max_id=%s',
            $endpoint,
            self::getSetting('access_token'),
            $this->getMaxResults(),
            $this->getMaxId()
        );

        if ($this->isDebugEnabled()) {
            Mage::log(__METHOD__ . ' :: requesting feed: ' . $endpoint);
        }

        $responseBody = $this->_sendRequest($endpoint);
        $feed = json_decode($responseBody, true);

        if (!is_array($feed) || !isset($feed['data'])) {
            if ($this->isDebugEnabled()) {
                Mage::log(__METHOD__ . ' :: Feed returned unexpected data: ' . var_export($responseBody, true), Zend_Log::ERR);
            }

            return false;
        } elseif ($this->isDebugEnabled()) {
            Mage::log(__METHOD__ . ' :: Feed returned: ' . count($feed['data']) . ' result(s)');
        }

        $this->setNumResults(count($feed['data']));
        if ($this->getNumResults() > 0 && $feed['pagination']['next_max_id']) {
            $this->setMaxId($feed['pagination']['next_max_id']);
        } else {
            $this->setMaxId(0);
        }

        return $feed['data'];
    }

    protected function _sendRequest($endpoint)
    {
        $data = false;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_URL, 'https://api.instagram.com/v1' . $endpoint);

        try {
            $data = curl_exec($curl);
        } catch (Exception $e) {
            if ($this->isDebugEnabled()) {
                Mage::logException($e);
            }
        }

        curl_close($curl);
        return $data;
    }
}