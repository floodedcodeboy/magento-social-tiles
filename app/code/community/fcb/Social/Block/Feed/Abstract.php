<?php

abstract class Fcb_Social_Block_Feed_Abstract extends Mage_Core_Block_Template
{

    abstract public function setPagerQueryParams(array $params);

    public function getPagerQueryParams()
    {
        return null;
    }

    public function getCacheKey()
    {
        return $this->getName() . '_' . md5(http_build_query($this->getPagerQueryParams()));
    }

    public function getCacheTags()
    {
        return array(constant('social_' . get_class($this) . '::CONFIG_GROUP'));
    }

    public function isEnabled()
    {
        return self::getSetting('enabled');
    }

    public static function isDebugEnabled()
    {
        return Fcb_Social_Block_Tiles::isDebugEnabled();
    }

    public static function getSetting($key, $group = null)
    {
        if ($group === null) {
            $group = constant(get_called_class() . '::CONFIG_GROUP');
        }

        return Fcb_Social_Block_Tiles::getSetting($key, $group);
    }
}