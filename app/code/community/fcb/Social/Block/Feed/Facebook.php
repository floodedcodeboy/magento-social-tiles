<?php

class Fcb_Social_Block_Feed_Facebook extends Fcb_Social_Block_Feed_Abstract
{
    const CONFIG_GROUP = 'facebook';

    public function setPagerQueryParams(array $params)
    {
        return $this;
    }

    public function getPagerQueryParams()
    {
        return array();
    }

    public function getFeed()
    {
        $feedUrl = sprintf(
            'https://graph.facebook.com/%s/feed?access_token=%s&limit=%s',
            self::getSetting('page_id'),
            self::getSetting('access_token'),
            $this->getLimit()
        );

        if ($this->isDebugEnabled()) {
            Mage::log(__METHOD__ . ' :: requesting feed: ' . $feedUrl);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_URL, $feedUrl);

        try {
            $feed = curl_exec($curl);
        } catch (Exception $e) {
            if ($this->isDebugEnabled()) {
                Mage::logException($e);
            }

            curl_close($curl);
            return false;
        }

        $feed = json_decode($feed, true);

        if (!isset($feed['data'])) {
            if ($this->isDebugEnabled()) {
                Mage::log(__METHOD__ . ' :: Feed returned unexpected data: ' . var_export($feed, true), Zend_Log::ERR);
            }

            return false;
        } elseif ($this->isDebugEnabled()) {
            Mage::log(__METHOD__ . ' :: Feed returned: ' . count($feed['data']) . ' result(s)');
        }

        $this->setNumResults(count($feed['data']));
        if ($this->getNumResults() > 0) {
            $this->setMaxId($feed['pagination']['next_max_id']);
        }

        return $feed['data'];
    }
}