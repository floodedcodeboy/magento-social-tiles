<?php

class Fcb_Social_Block_Feed_Twitter extends Fcb_Social_Block_Feed_Abstract
{
    const CONFIG_GROUP = 'twitter';
    const PAGER_PARAM = 'twitter_max_id';

    protected $_maxResults = 24;
    protected $_numResults;
    protected $_maxId;
    protected $_username;
    protected $_includeRts = false;
    protected $_excludeReplies = true;

    public function __construct()
    {
        $libDir = Mage::getModuleDir('', 'fcb_Social') . '/lib';
        require_once $libDir . '/jublonet/codebird-php/src/codebird.php';

        parent::__construct();
    }

    public function setPagerQueryParams(array $params)
    {
        if (isset($params[self::PAGER_PARAM])) {
            $this->setMaxId($params[self::PAGER_PARAM]);
        }

        return $this;
    }

    public function getPagerQueryParams()
    {
        return array(self::PAGER_PARAM => $this->getMaxId());
    }

    public function setUsername($username)
    {
        $this->_username = $username;
        return $this;
    }

    public function getUsername()
    {
        if (!isset($this->_username)) {
            $this->_username = self::getSetting('username');
        }

        return $this->_username;
    }

    public function setNumResults($numResults)
    {
        $this->_numResults = $numResults;
        return $this;
    }

    public function getNumResults()
    {
        return $this->_numResults;
    }

    public function setMaxId($maxId)
    {
        $this->_maxId = $maxId;
        return $this;
    }

    public function getMaxId()
    {
        return $this->_maxId;
    }

    public function setMaxResults($maxResults)
    {
        $this->_maxResults = $maxResults;
        return $this;
    }

    public function getMaxResults()
    {
        return $this->_maxResults;
    }

    public function setIncludeRetweets($includeRts)
    {
        $this->_includeRts = $includeRts;
        return $this;
    }

    public function getIncludeRetweets()
    {
        return $this->_includeRts;
    }

    public function setExcludeReplies($excludeReplies)
    {
        $this->_excludeReplies = $excludeReplies;
        return $this;
    }

    public function getExcludeReplies()
    {
        return $this->_excludeReplies;
    }

    public function getTweets()
    {
        $tweets = false;

        try {
            $options = array(
                'count' => $this->getMaxResults(),
                'screen_name' => $this->getUsername(),
                'include_rts' => $this->getIncludeRetweets(),
                'exclude_replies' => $this->getExcludeReplies()
            );

            if ($this->getMaxId() != null) {
                $options['max_id'] = intval($this->getMaxId()) - 1;
            }

            if ($this->isDebugEnabled()) {
                Mage::log(__METHOD__ . ' :: requesting feed: statuses/user_timeline?' . http_build_query($options));
            }

            $response = $this->_getClient()->statuses_userTimeline(http_build_query($options));

            $count = 0;
            if (is_object($response) && isset($response->httpstatus) && $response->httpstatus == 200) {
                $tweets = array();
                $responseArray = (array) $response;

                foreach ($responseArray as $key => $data) {
                    if (is_numeric($key)) {
                        $tweets[] = $data;
                        $count++;
                    }
                }
            }

            $this->setNumResults($count);

            if ($count > 0) {
                $this->setMaxId($tweets[$count - 1]->id);
            } else {
                $this->setMaxId(0);
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }

        return $tweets;
    }

    public function searchTweets($query)
    {
        $tweets = false;

        try {
            $options = array(
                'q' => $query,
                'count' => $this->getMaxResults()
            );

            if ($this->getMaxId()) {
                $options['max_id'] = intval($this->getMaxId()) - 1;
            }

            $response = $this->_getClient()->search_tweets(http_build_query($options));
            $count = $response->search_metadata->count;
            $this->setNumResults($count);
            if ($count > 0) {
                $tweets = $response->statuses;
                $this->setMaxId($response->search_metadata->max_id);
            } else {
                $this->setMaxId(0);
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }

        return $tweets;
    }

    protected function _getClient()
    {
        $appKey = self::getSetting('app_key');
        $appSecret = self::getSetting('app_secret');
        $token = self::getSetting('access_token');
        $tokenSecret = self::getSetting('token_secret');

        \Codebird\Codebird::setConsumerKey($appKey, $appSecret);
        $cb = \Codebird\Codebird::getInstance();
        $cb->setToken($token, $tokenSecret);

        return $cb;
    }
}
