<?php

class Fcb_Social_TilesController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();

        $blockName = $this->getRequest()->getParam('name', 'social.tiles');
        $block = $this->getLayout()->getBlock($blockName);

        $this->getResponse()->setBody(utf8_encode($block->toHtml()));
    }
}