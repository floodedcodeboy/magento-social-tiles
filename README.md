magento-social-tiles
====================
This magento extension provides a simple way to collect and aggregate media from multiple social media feeds, including Instagram, Twitter, Vimeo, and Facebook, and display them as a grid of tiles in your Magento store.

A live example can be seen at http://ethika.com/social, where the extension is used in conjunction with Isotope (http://isotope.metafizzy.co/) to add sorting and filtering. A simple example can be seen at http://yeanice.com/instagram
